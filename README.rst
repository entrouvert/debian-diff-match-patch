================
diff-match-patch
================

Documentation on the Google site:

http://code.google.com/p/google-diff-match-patch/

See README.original.txt for the original README.

This is an unofficial package of the Python versions for PyPI, by
Luke Plant http://lukeplant.me.uk/

See https://bitbucket.org/spookylukey/diff-match-patch for packaging issues.



